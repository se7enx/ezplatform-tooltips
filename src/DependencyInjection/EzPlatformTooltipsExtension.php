<?php

namespace ContextualCode\EzPlatformTooltips\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class EzPlatformTooltipsExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->prependExtension($container, 'ezrichtext');
    }

    protected function prependExtension(ContainerBuilder $container, string $extension): void
    {
        $configFile = __DIR__ . '/../Resources/config/' . $extension . '.yaml';
        $container->prependExtensionConfig($extension, Yaml::parseFile($configFile));
        $container->addResource(new FileResource($configFile));
    }
}
