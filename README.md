# eZ Platform Tooltips

eZ Platform bundle which provides few fixes for tooltips:

- Disables CKEditor acceptability tooltips
- Disables rich text fields tooltips, based on field description
- Disables acceptability tooltips for custom tag icons (only in case if custom tooltips are enabled)

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-tooltips
    ```

2. Clear browser caches and enjoy!
